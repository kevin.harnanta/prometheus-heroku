#!/bin/ash
echo "LORDCHOU_PROMETHEUS is starting up . . . "
echo "Your prometheus port $PORT"

sed -i -e "s/corona-exporter-hostname/$CORONA_EXPORTER_HOSTNAME/g" /etc/prometheus/prometheus.yml
sed -i -e "s/corona-exporter-port/$CORONA_EXPORTER_PORT/g" /etc/prometheus/prometheus.yml

/bin/prometheus --config.file=/etc/prometheus/prometheus.yml --storage.tsdb.path=/prometheus --web.console.libraries=/usr/share/prometheus/console_libraries --web.console.templates=/usr/share/prometheus/consoles --web.listen-address=0.0.0.0:${PORT}
